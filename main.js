// Функція для здійснення AJAX запиту
function fetchStarWarsData(url) {
    return fetch(url)
        .then(response => response.json())
        .catch(error => console.error('Error:', error));
}

// Отримання фільмів серії "Зоряні війни"
fetchStarWarsData('https://ajax.test-danit.com/api/swapi/films')
    .then(data => {
        console.log(data);
        const filmsList = document.getElementById('films-list');
        data.forEach(film => {
            const { episodeId: episodeId, name: name, openingCrawl: openingCrawl, characters: characters } = film;
            const filmItem = document.createElement('li');
            filmItem.innerHTML = `<div class="episod">Episode ${episodeId}:</div> <div class="name">film name: ${name} </div>    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>            - <div class="openingCrawl">${openingCrawl}</div>`;
            filmsList.appendChild(filmItem);
            const character = Promise.all(
                characters.map((char) => fetchStarWarsData(char))
            )
                .then(charactersData => {
                    filmItem.getElementsByClassName('lds-ring')[0].style.display = 'none';
                    console.log(charactersData);
                    const characterItem = document.createElement('p');
                    charactersData.forEach((character) => {
                        characterItem.textContent = characterItem.textContent+", "+character.name;
                        filmItem.getElementsByClassName('openingCrawl')[0].insertAdjacentElement('beforebegin', characterItem);
                    });
                });

        });
    });